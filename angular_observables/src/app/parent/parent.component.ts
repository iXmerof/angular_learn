import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { HttpService } from '../http.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  @ViewChild('alertElement')
  alertElement: ElementRef;

  private accessGranted$: Observable<boolean>;

  lastError: string;

  constructor(private http: HttpService) {
    const alert$ = of(this.alertElement.nativeElement, 'keyup');
    this.http.getAccess(alert$, this.onHttpError);
  }

  onHttpError(errorMessage) {
    this.lastError = errorMessage;
  }

  ngOnInit() {
  }

  onGetClass() {
    this.http.getClassName();
  }

}
