import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) { }

  getClassName(): string {
    return 'alert alert-primary';
  }

  getAccess(clickEvent: Observable<any>, errorHandler): Observable<boolean> {

    const response: Subject<boolean> = new Subject<boolean>();

    clickEvent.subscribe( data => {

    const headers: HttpHeaders = new HttpHeaders();
    headers.append('content-type', 'json');

    const params: HttpParams = new HttpParams();
    params.append('value', data);

    this.httpClient.get<boolean>('localhost:3000/class', {headers: headers})
    .subscribe(res => response.next(res), err => errorHandler(err));

    });

    return response.asObservable();
  }
}
