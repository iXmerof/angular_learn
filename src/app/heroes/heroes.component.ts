import { Component, OnInit } from '@angular/core';
import { HeroService } from '../hero.service'
import { MessagesService } from '../messages.service'
import { Hero } from '../hero';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

    selectedHero: Hero;
    heroes : Hero[];

  constructor(private heroService: HeroService, private messagesService: MessagesService) { }

  ngOnInit() { this.getHeroes(); }

  getHeroes() : void
  {
    this.heroService.GetHeroes().subscribe(_heroes => this.heroes = _heroes);
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
    this.messagesService.Add("Selected new hero: " + hero.name);
  }
}
