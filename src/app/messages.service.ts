import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  messages : string[] = [];

  constructor() { }

  Add (message: string)
  {
    this.messages.push(message);
  }

  Clear() {
    this.messages = [];
}
}
