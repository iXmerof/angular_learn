import { Injectable } from '@angular/core';
import { HEROES } from './mock-heroes'
import { Hero } from './hero'
import { Observable, of } from '../../node_modules/rxjs';
import { MessagesService } from './messages.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

   constructor(private message: MessagesService) { }

   GetHeroes() : Observable<Hero[]>
   {
    this.message.Add("Got heroes list");
     return of(HEROES);
   }

   GetHero(id: number) : Observable<Hero>
   {
    this.message.Add(`Got hero of id ${id}`);
     return of(HEROES.find(hero => hero.id === id))
   }
}
