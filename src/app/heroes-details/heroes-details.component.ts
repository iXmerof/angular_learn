import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero'
import { HeroService } from "../hero.service"
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroes-details',
  templateUrl: './heroes-details.component.html',
  styleUrls: ['./heroes-details.component.css']
})
export class HeroesDetailsComponent implements OnInit {
  
  @Input() hero: Hero;

  constructor(private heroService: HeroService,
              private route: ActivatedRoute) { }

  ngOnInit() 
  {
    this.getHero();
  }

  getHero()
  {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.GetHero(id).subscribe(_hero => this.hero = _hero);
  }
}
